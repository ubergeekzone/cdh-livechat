import { Component } from 'preact';

import { createClassName } from '../helpers';
import styles from './styles.scss';


export class Avatar extends Component {
	state = {
		errored: false,
	}

	handleError = () => {
		this.setState({ errored: true });
	}

	componentWillReceiveProps({ src: nextSrc }) {
		const { src } = this.props;
		if (nextSrc !== src) {
			this.setState({ errored: false });
		}
	}

	checkSRC = (src, description) => {
	var cdh = src.includes("Crossdresser Heaven");
	var tgh = src.includes("Transgender Heaven");
	var jsonStore = JSON.parse(localStorage.getItem("store"));
	if (cdh) {
  	   src = 'https://www.crossdresserheaven.com/wp-content/uploads/2015/11/ipad-retina.png';
	} else if (tgh) {
  	   src = 'https://transgenderheaven.com/wp-content/uploads/2018/06/tgh-fav-icon-144.png';
	}  else {
		 if(jsonStore.lcGuestAvatar !== "0" && description.includes("guest")) {
			 src = jsonStore.lcGuestAvatar;
		 } else {
			 src = src;
		 }
	}
	return src;
	}

	render = ({ small, large, src, description, status, className, style }, { errored }) => (

		<div
			aria-label="User picture"
			className={createClassName(styles, 'avatar', { small, large, nobg: src && !errored }, [className])}
			style={style}
		>
			{(src && !errored) && (
				<img
					src={this.checkSRC(src, description)}
					alt={description}
					className={createClassName(styles, 'avatar__image')}
					onError={this.handleError}
				/>
			)}

			{status && (
				<span className={createClassName(styles, 'avatar__status', { small, large, status })} />
			)}
		</div>
	)
}
